package com.alexmegremis.challenges.hotelsdotcom.parsers;

/**
 * Implementations are parsers of NT lineitems.
 * <p>
 * Created by alexmegremis on 26/06/2016.
 */
public interface INTripleURLParser {

    String REGEX_LOOSE = ".*<(.*)>.*";
    String REGEX_STRICT_GROUPED = "<(.*?)?>.*?<(.*)>.*<(.*)>.*";
    String REGEX_STRICT = "<.*?>.*?<.*>.*<.*>.*";

    /**
     * Returns the third column of an N-Triples lineitem.
     *
     * @param lineItem
     * @return
     */
    String parseFromNTriple(final String lineItem);

}
