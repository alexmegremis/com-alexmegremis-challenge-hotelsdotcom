package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Paths;

/**
 * {@inheritDoc}
 * <p>
 * This implementation will communicate with a {@link INTripleLineHandler} instance.
 * It passes each line it reads, and eventually notifies when there are no more lines
 * in the read file.
 * <p>
 * Created by alexmegremis on 27/06/2016.
 */
@Component
public class FileReader implements IFileReader {

    @Autowired
    INTripleLineHandler lineHandler;
    @Value("${path.ntfile}")
    private String path;

    /**
     * {@inheritDoc}
     * <p>
     * This implementatoion will read file contents as lines and continuously
     * pass them on to a {@link INTripleLineHandler} instance. There is no
     * throttling here. It assumes the receiving end can handle the content
     * being pushed, or we are simply made to wait.
     *
     * @throws IOException
     */
    @Override
    public void begin() throws IOException {

        String line;

        InputStream inputStream = new FileInputStream(Paths.get(path).toFile());
        Reader reader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(reader);

        while ((line = bufferedReader.readLine()) != null) {
            try {
                lineHandler.handle(line);
//                System.out.println(">> pushed line " + line);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(">> file done");
        lineHandler.done();
        inputStream.close();
    }
}
