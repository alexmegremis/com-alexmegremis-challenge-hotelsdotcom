package com.alexmegremis.challenges.hotelsdotcom.config;

import com.alexmegremis.challenges.hotelsdotcom.api.v1.controllers.StatsController;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.springframework.context.annotation.Configuration;

/**
 * Created by alexmegremis on 26/06/2016.
 */
@Configuration
public class AppConfig extends ResourceConfig {

    public AppConfig() {

        register(WadlResource.class);
        register(ApiListingResource.class);

        register(StatsController.class);

        configureSwagger();
    }

    private void configureSwagger() {
        register(ApiListingResource.class);
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
//        beanConfig.setSchemes(new String[]{"http"});
//        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/apiDocs");
        beanConfig.setResourcePackage("com.alexmegremis.challenges.hotelsdotcom");
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
    }

}
