package com.alexmegremis.challenges.hotelsdotcom.parsers;

import org.springframework.stereotype.Component;

/**
 * Created by alexmegremis on 26/06/2016.
 */
@Component
public class NTripleURLParser implements INTripleURLParser {

    /**
     * {@inheritDoc}
     * <p>
     *     We specifically expect lineitems in the format of:
     * <p>
     *     &lt;URL&gt; &lt;URL&gt; &lt;URL&gt; .
     * <p>
     *     The format is given to us as fixed, so we won't bother
     * with too many safety checks, otherwise we'd be going with a
     * regex group approach instead.
     */
    public String parseFromNTriple(final String lineItem) {

        String result;

        int startIndex = lineItem.lastIndexOf('<') + 1;

        // The dataset seems to guarantee a line finish of "> .", so lets save a bit more time
        // int endIndex = aLine.lastIndexOf('>');
        int endIndex = lineItem.length() - 3;

        // Some very shoddy checking - we're not asked to be paranoid
        if (startIndex > -1 && startIndex < endIndex) {
            result = lineItem.substring(startIndex, endIndex);
        } else {
            throw new IllegalArgumentException("No identifiable URL column was found: " + lineItem);
        }

        return result;
    }

}
