package com.alexmegremis.challenges;

import java.util.concurrent.*;

/**
 * Created by alexmegremis on 28/06/16.
 */
public class QueueTest {
    private static final BlockingQueue<String> queue = new ArrayBlockingQueue<>(5);

    public static void main(String[] args) throws InterruptedException {

        Runnable foo = new RunnableFuture<String>() {

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return false;
            }

            @Override
            public String get() throws InterruptedException, ExecutionException {
                return null;
            }

            @Override
            public String get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return null;
            }

            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                queue.add("Hello world");
            }
        };

        System.out.println(">> checking");
        foo.run();

        String message = queue.poll(5, TimeUnit.SECONDS);
        System.out.println(message);
        System.out.println(">> done");
    }
}
