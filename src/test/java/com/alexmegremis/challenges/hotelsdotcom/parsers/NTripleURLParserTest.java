package com.alexmegremis.challenges.hotelsdotcom.parsers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by alexmegremis on 26/06/2016.
 */
public class NTripleURLParserTest {

    NTripleURLParser parser;

    public static final String LINE_SIMPLEURL_VALID = "<http://dbpedia.org/resource/Canidae> <http://dbpedia.org/ontology/wikiPageExternalLink> <http://www.awdconservancy.org/> .";
    public static final String URL_SIMPLE = "http://www.awdconservancy.org/";

    public static final String LINE_COMPLEXURL_VALID = "<http://dbpedia.org/resource/Cuisine> <http://dbpedia.org/ontology/wikiPageExternalLink> <http://books.google.com/books?id=zG1H75z0EYYC&pg=RA2-PR10&dq=cuisine+encyclopedia&hl=en&ei=45OmTofjO8aIiALZtLHMDQ&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDwQ6AEwAQ#v=onepage&q=cuisine%20encyclopedia&f=false> .";
    public static final String URL_COMPLEX = "http://books.google.com/books?id=zG1H75z0EYYC&pg=RA2-PR10&dq=cuisine+encyclopedia&hl=en&ei=45OmTofjO8aIiALZtLHMDQ&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDwQ6AEwAQ#v=onepage&q=cuisine%20encyclopedia&f=false";

    public static final String LINE_EMPTYURL_INVALID = "<http://dbpedia.org/resource/Canidae> <http://dbpedia.org/ontology/wikiPageExternalLink> <> .";

    @Before
    public void setUp() throws Exception {
        parser = new NTripleURLParser();
    }

    @After
    public void tearDown() throws Exception {
        parser = null;
    }

    @Test
    public void parseFromNTriple_simpleURL_valid() throws Exception {
        String actual = parser.parseFromNTriple(LINE_SIMPLEURL_VALID);
        assertEquals(URL_SIMPLE, actual);
    }

    @Test
    public void parseFromNTriple_complexURL_valid() throws Exception {
        String actual = parser.parseFromNTriple(LINE_COMPLEXURL_VALID);
        assertEquals(URL_COMPLEX, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseFromNTriple_emptyURL_invalid() throws Exception {
        parser.parseFromNTriple(LINE_EMPTYURL_INVALID);
    }

}