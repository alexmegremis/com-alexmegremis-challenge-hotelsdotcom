package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import com.alexmegremis.challenges.hotelsdotcom.api.v1.dto.StatsDTO;

/**
 * Implementations are classes to manage statistics for URL processing.
 * <p>
 * Created by alexmegremis on 27/06/16.
 */
public interface IStatsHolder {

    /**
     * Record the URL test result in our counters.
     *
     * @param URL
     * @param result
     */
    void addTestResult(String URL, String result);

    /**
     * Produce a populated DTO with statistics on ongoing processing.
     *
     * @return
     */
    StatsDTO getResponseCodeStats();

    /**
     * Produce a count for a specific http code.
     *
     * @return
     */
    Integer getResponseCodeStatsForCode(final String code);
}
