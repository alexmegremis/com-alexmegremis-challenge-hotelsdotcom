package com.alexmegremis.challenges.hotelsdotcom.urlstats;

/**
 * Created by alexmegremis on 27/06/16.
 */
public interface IURLTester extends Runnable {

    String CODE_UNKNOWN_HOST = "UNKNOWN_HOST";
    String CODE_BAD_URL = "BAD_URL";
    String CODE_TIMEOUT = "CONNECTION_TIMEOUT";
    String CODE_RESET = "CONNECTION_RESET";
    String CODE_FTP_URL = "NOT_HTTP_URL";
    String CODE_MISCELLANEOUS_ERROR = "MISCELLANEOUS_ERROR";

    /**
     * Perform activities on a HTTP(S) URL, provided in full form.
     *
     * @param nextURL
     */
    void testURL(String nextURL);
}
