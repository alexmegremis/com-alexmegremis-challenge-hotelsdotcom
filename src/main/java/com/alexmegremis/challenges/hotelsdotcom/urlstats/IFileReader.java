package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import java.io.IOException;

/**
 * Readers for files.
 * <p>
 * Created by alexmegremis on 26/06/2016.
 */
public interface IFileReader {
    /**
     * This will have the reader begin reading the file it's aimed at.
     */
    void begin() throws IOException;
}
