The implementation requires:
1. Java 8 (use of a lambda)
2. Maven 3.3.x
3. An internet connection (build and execution)

Having exploded the tarball, `cd` into the project folder and do
`mvn clean package`

Once completed, `cd` into the target folder and do:
`java -jar HotelsDotCom-1.0.0-SNAPSHOT.jar --ntfile=<path-to-NT-file>`
for example
`java -jar HotelsDotCom-1.0.0-SNAPSHOT.jar --ntfile=../external_links_en.nt`

Things to note:

# Threading
While not specified in the requirements, the application is multithreaded. The bulk of the logic, and the file reader, run on the main thread. The URL testing is done on multiple worker threads.
Threading is a topic I still have limited experience with, but the design choice was obvious. 

# Spring Boot
The application is a _**Spring Boot**_ implementation, from which execution and REST API capabilities are provided.

# Configuration
Due to time restrictions, the only parameter exposed to the user is the location of the NT file. Aspects such as the http port for the API, or the number of worker threads, are hardcoded.

# REST API
The URL to query the API is slightly different to what was specified. To somewhat observe best practices, the URL now is:
 http://localhost:8090/api/statistics/all
Replace all with any HTTP code to receive a hit count on that, such as
 http://localhost:8090/api/statistics/302 
API responses are normally HTTP 200. If a HTTP code requested has zero hits, a 404 is returned with the number.

# Runtime
Once the NT file execution is complete, the URL testing threads will slowly shutdown. However, the API remains until the application is terminated.

# Testing
The bulk of the application is a matter of wiring components together, so testing may seem somewhat limited as I didn't go into writing tests to be run with Spring's runner. Again, time restrictions.

# Missed feature
Requirement [6] - write the <title> of each HTTP200 page with metadata to a file, was not implemented. Time restrictions.

# Final thoughts
This was a fun challenge to go through.
I even went down the path of benchmarking what is the fastest way to extract a URL out of an NTriple line. Funnily enough, while any regex was orders of magnitude slower than any index-based strategy, the rest of the benchmarks turned out to produce totally inconsistent results, depending on where they are running.

Note that the application was not written tests-first. There were technical challenges that I focused on first, as I knew the types of capabilities I would need. Once these started taking form, then the design for the flow of the application was laid down, and tests were written.
The test coverage itself is by no means exhaustive. Virtually no edge cases are covered, apart from the wide range of HTTP results, as the scope of the challenge doesn't seem to have too many edges to watch out for.

(Yes, I could test what happens if the actual net connection drops when the application is running - spoiler alert, the connections are just recorded as timed out at Ludicrous Speed, but that's a bit outside the scope of a challenge)


==================================================================================
Time: 3 hours
Language: Java 7+

# Description
This exercise is about building a simple program which processes a set of URLs, performs an HTTP GET request to them, reads the data and exposes statistics on the ongoing process.

First thing, download the [dataset][1]. It’s in NT [format][2] but you are only interested in the third column - the one containing the URLs. For example, given the first row, you're only interested in the url http://www.albedo-project.org/.

The data set is approximately 135 MB, so if you have a slow connection, please use the [tiny extract of it][3].

Take your time to get familiar with the data structure and make sure you understand how to parse it.

# Hints

Keep in mind that we love testable code that can be run easily. Don’t worry if you can’t cover all the requirements, but make sure that your code has unit tests and is well designed.

Our projects generally use Maven and Spring. You do not have to use these for the test, but please note that you will be expected to demonstrate familiarity with these technologies in the interview.

# Requirements
Write a java standalone application with the following requirements and characteristics

1. Reads the [dataset][1] in, in streaming fashion, and for each URL makes an HTTP GET request to that URL.
2. Keeps a counter for each HTTP return status code (200 OK, 404 etc), returned from all the HTTP requests.

3. Exposes a REST endpoint `/status` that returns a break down of the current HTTP status (how many 200s, how many 404) each time it is called.
4. Exposes a REST endpoint `/status/{httpReturnCode}` that returns the count for the specified HTTP status code.
5. Returns JSON from both REST endpoints.

Please include a simple README.md on how to run and execute it, compress your source code and the README and send it to us.

## If you have extra time left over, there are some optional requirements:
6. For each URL called which returns a 200 OK, parse the HTML page, extract the HTML `<title>` element and append the couple `<url-of-the-page, title>` to a local file
7. Add an element to the breakdown of the endpoint requested at item (3), containing the latest processed url and a timestamp of when it has been processed.

Enjoy!

Hotels.com

# Appendix
* [1]: http://downloads.dbpedia.org/3.9/en/external_links_en.nt.bz2
* [2]: https://en.wikipedia.org/wiki/N-Triples
* [3]: http://downloads.dbpedia.org/preview.php?file=3.9_sl_en_sl_external_links_en.nt.bz2
