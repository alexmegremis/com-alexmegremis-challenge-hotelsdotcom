package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import com.alexmegremis.challenges.hotelsdotcom.parsers.INTripleURLParser;
import com.alexmegremis.challenges.hotelsdotcom.parsers.NTripleURLParserTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static org.mockito.Mockito.*;

/**
 * Created by alexmegremis on 27/06/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class NTripleLineHandlerTest {

    @Mock
    private BlockingQueue<String> mockUrlQueue;
    @Mock
    private INTripleURLParser mockUrlParser;
    @Mock
    private IURLTester mockURLTester;
    List<Future> futures;
    @Mock
    private ExecutorService executorService;

    INTripleLineHandler handler;
    NTripleLineHandler concreteRef;

    @Before
    public void setUp() throws Exception {

        when(mockUrlParser.parseFromNTriple(NTripleURLParserTest.LINE_COMPLEXURL_VALID))
                .thenReturn(NTripleURLParserTest.URL_COMPLEX);

        handler = new NTripleLineHandler();

        futures = new ArrayList<>();

        concreteRef = (NTripleLineHandler) handler;
        concreteRef.setUrlParser(mockUrlParser);
        concreteRef.setUrlQueue(mockUrlQueue);
        concreteRef.setExecutorService(executorService);
    }

    @After
    public void tearDown() throws Exception {
        mockUrlQueue = null;
        mockUrlParser = null;
        mockURLTester = null;
        futures = null;
        executorService = null;

        concreteRef = null;
        handler = null;
    }

    @Test
    public void testHandle_urlAddedToQueue() throws Exception {
        handler.handle(NTripleURLParserTest.LINE_COMPLEXURL_VALID);
        verify(mockUrlQueue, times(1)).put(NTripleURLParserTest.URL_COMPLEX);
    }

    @Test
    public void testInit_workerThreadsAdded() throws Exception {
        concreteRef.init();
        verify(executorService, times(NTripleLineHandler.THREAD_COUNT)).submit(Mockito.any(IURLTester.class));
    }

    @Test
    public void done() throws Exception {

    }

}