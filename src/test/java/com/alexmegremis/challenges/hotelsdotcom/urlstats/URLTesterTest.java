package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import com.alexmegremis.challenges.hotelsdotcom.parsers.NTripleURLParserTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.concurrent.BlockingQueue;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by alexmegremis on 28/06/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class URLTesterTest {

    @Mock
    private BlockingQueue<String> mockUrlQueue;
    @Mock
    private IStatsHolder mockStatsHolder;

    IURLTester urlTester;

    public static final String URL_FTP_VALID = "ftp://ftp.centos.org";
    public static final String URL_NOT_FOUND = "http://www.google.co.uk/awesomesauce";
    public static final String URL_MALFORMED = "htp://www.google.co.uk/awesomesauce";
    public static final String URL_NO_PROTOCOL = "www.google.co.uk/awesomesauce";
    public static final String URL_UNKNOWN_HOST = "http://www.googlebutnotreallygoogle.co.uk";
    // we assume this won't be used...
    public static final String URL_TIMEOUT = "http://localhost:20999";

    @Before
    public void setUp() throws Exception {
        urlTester = new URLTester(mockUrlQueue, mockStatsHolder);
    }

    @After
    public void tearDown() throws Exception {
        urlTester = null;
    }

    @Test
    public void testTestURL_validHttpURL_status200Recorded() throws Exception {
        urlTester.testURL(NTripleURLParserTest.URL_SIMPLE);
        verify(mockStatsHolder, times(1)).addTestResult(NTripleURLParserTest.URL_SIMPLE, String.valueOf(Response.Status.OK.getStatusCode()));
    }

    @Test
    public void testTestURL_validFtpURL_statusCustomNotHttpUrlRecorded() throws Exception {
        urlTester.testURL(URL_FTP_VALID);
        verify(mockStatsHolder, times(1)).addTestResult(URL_FTP_VALID, IURLTester.CODE_FTP_URL);
    }

    @Test
    public void testTestURL_notFoundURL_status404Recorded() throws Exception {
        urlTester.testURL(URL_NOT_FOUND);
        verify(mockStatsHolder, times(1)).addTestResult(URL_NOT_FOUND, String.valueOf(Response.Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void testTestURL_malformedURL_statusCustomBadUrlRecorded() throws Exception {
        urlTester.testURL(URL_MALFORMED);
        verify(mockStatsHolder, times(1)).addTestResult(URL_MALFORMED, IURLTester.CODE_BAD_URL);
    }

    @Test
    public void testTestURL_noProtocolURL_statusCustomBadUrlRecorded() throws Exception {
        urlTester.testURL(URL_NO_PROTOCOL);
        verify(mockStatsHolder, times(1)).addTestResult(URL_NO_PROTOCOL, IURLTester.CODE_BAD_URL);
    }

    @Test
    public void testTestURL_unknownHostURL_statusCustomUnknownHostRecorded() throws Exception {
        urlTester.testURL(URL_UNKNOWN_HOST);
        verify(mockStatsHolder, times(1)).addTestResult(URL_UNKNOWN_HOST, IURLTester.CODE_UNKNOWN_HOST);
    }

    @Test
    public void testTestURL_nullURL_statusCustomMiscellaneousRecorded() throws Exception {
        urlTester.testURL(null);
        verify(mockStatsHolder, times(1)).addTestResult(null, IURLTester.CODE_BAD_URL);
    }
}