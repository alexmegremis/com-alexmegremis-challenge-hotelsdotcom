package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import com.alexmegremis.challenges.hotelsdotcom.parsers.INTripleURLParser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * This implementation will accept NTriple lines and process them asynchronously
 * in worker threads.
 * <p>
 * This class, for this exercise, orchestrates all the activity, as it interacts
 * with handlers for reading files, processing lines from these files, doing external
 * activities, and statistics.
 * <p>
 * Created by alexmegremis on 27/06/16.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class NTripleLineHandler implements INTripleLineHandler {

    @Autowired
    @Getter @Setter
    private IFileReader fileReader;
    @Autowired
    @Getter @Setter
    private INTripleURLParser urlParser;
    @Autowired
    private IStatsHolder statsHolder;

    @Getter @Setter
    private ExecutorService executorService;
    @Getter @Setter
    private BlockingQueue<String> urlQueue;

    // @TODO parameterize this
    public static final int THREAD_COUNT = 200;

    @Getter @Setter
    private List<Future> futures;

    public NTripleLineHandler() {
    }

    public NTripleLineHandler(final BlockingQueue<String> urlQueue) {
        this.urlQueue = urlQueue;
    }

    /**
     * Set defaults, if not already initialized, and kick off the worker threads for
     * URL validation.
     */
    @PostConstruct
    public void init() {
        if (urlQueue == null) {
            this.urlQueue = new ArrayBlockingQueue<>(THREAD_COUNT * 2);
        }
        if (this.executorService == null) {
            this.executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        }

        if (futures == null) {
            futures = new ArrayList<>(THREAD_COUNT);
            for (int i = 0; i < THREAD_COUNT; i++) {
                Future future = executorService.submit(new URLTester(urlQueue, statsHolder));
                futures.add(future);
            }
        }
    }

    /**
     * Extract a URL (as text) from a line and add it to the queue for processing.
     * <p>
     * Note: Since we've gone with a multithreaded model, depending on what the
     * concurrent java namespace gives for containers, this method really has shrunk
     * to just this.
     *
     * @param lineItem
     */
    @Override
    public void handle(final String lineItem) throws InterruptedException {
        String URL = urlParser.parseFromNTriple(lineItem);
        urlQueue.put(URL);
    }

    @Override
    public void done() {
//        System.out.println(">> Going to kill workers");
        while (urlQueue.size() > 0) {
//            System.out.println(">> delaying, still URLs in the queue");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // @TODO do something! press a button! call someone!
            }
        }

//        System.out.println(">> Starting to kill workers");
        for (Future aFuture : futures) {
//            System.out.println(">> DIE!!");
            aFuture.cancel(true);
        }
//        System.out.println(">> Blood bath over.");
    }
}
