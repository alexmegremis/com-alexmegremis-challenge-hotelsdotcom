package com.alexmegremis.challenges.hotelsdotcom.urlstats;

/**
 * Created by alexmegremis on 27/06/16.
 */
public interface INTripleLineHandler {
    /**
     * Consumer method for lines read from N-Triple extract.
     * Fire and forget.
     *
     * @param lineItem
     */
    void handle(final String lineItem) throws InterruptedException;

    /**
     * Let us know when there are no more rows left.
     */
    void done();
}
