package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import com.alexmegremis.challenges.hotelsdotcom.api.v1.dto.ResponseCodeStatDTO;
import com.alexmegremis.challenges.hotelsdotcom.api.v1.dto.StatsDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * This is our results service
 * Created by alexmegremis on 27/06/16.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class StatsHolder implements IStatsHolder {

    @Getter @Setter
    private Map<String, Integer> responseStatusMap = new HashMap<>();
    @Getter @Setter
    private String lastURLTested;
    @Getter @Setter
    private long lastURLTestTimestamp;
    @Getter @Setter
    private long totalCounter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void addTestResult(final String URL, final String result) {
        synchronized (this) {
            this.lastURLTested = URL;
            lastURLTestTimestamp = Calendar.getInstance().getTimeInMillis();
            Integer currentCount = responseStatusMap.get(result);
            responseStatusMap.put(result, currentCount == null ? 1 : ++currentCount);
            totalCounter++;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * This implementation returns a copy of stats data.
     * The codes list is sorted alphanumerically.
     */
    @Override
    public StatsDTO getResponseCodeStats() {

        StatsDTO result = new StatsDTO();

        List<ResponseCodeStatDTO> codeStats = new ArrayList<>();
        result.setCodeStats(codeStats);

        synchronized (this) {
            for (String aCode : responseStatusMap.keySet()) {
                ResponseCodeStatDTO aCodeStat = new ResponseCodeStatDTO(aCode, responseStatusMap.get(aCode));
                codeStats.add(aCodeStat);
            }

            result.setLastURLaccessed(this.lastURLTested);
            result.setLastURLaccessTime(this.lastURLTestTimestamp);
            result.setTotalURLsProcessed(totalCounter);
        }

        Collections.sort(codeStats, (stat01, stat02) -> stat01.getCode().compareTo(stat02.getCode()));
        return result;
    }

    /**
     * {@inheritDoc}
     * <p>
     * This implementation returns a copy of stats data, but only
     * a counter for a specific code.
     * We're being a bit lazy here
     * @param code
     * @return
     */
    @Override
    public Integer getResponseCodeStatsForCode(String code) {
        Integer result = 0;
        if(responseStatusMap.containsKey(code)) {
            result = responseStatusMap.get(code);
        }
        return result;
    }
}
