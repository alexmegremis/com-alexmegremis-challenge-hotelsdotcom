package com.alexmegremis.challenges.hotelsdotcom.api.v1.controllers;

import com.alexmegremis.challenges.hotelsdotcom.urlstats.IStatsHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by alexmegremis on 26/06/2016.
 */
@Component
@Path("/statistics")
public class StatsController {

    @Autowired
    IStatsHolder statsHolder;

    /**
     * Produce a response with all our stats.
     *
     * @return
     */
    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        Response result = getResponse(statsHolder.getResponseCodeStats(), Response.Status.OK);
        return result;
    }

    /**
     * Produce a count for a single HTTP code.
     *
     * @param code
     * @return
     */
    @GET
    @Path("/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getForCode(@PathParam("code") final String code) {
        Response result;
        Integer count = statsHolder.getResponseCodeStatsForCode(code);

        result = getResponse(count, Response.Status.OK);

        return result;
    }

    private Response getResponse(final Object payload, final Response.Status status) {
        Response response = Response.status(status)
                .entity(payload)
                .build();

        return response;
    }
}
