package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import com.alexmegremis.challenges.hotelsdotcom.api.v1.dto.StatsDTO;
import com.alexmegremis.challenges.hotelsdotcom.parsers.NTripleURLParserTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by alexmegremis on 28/06/16.
 */
public class StatsHolderTest {

    private IStatsHolder statsHolder;
    private StatsHolder concreteRef;
    private Map<String, Integer> responseStatusMap;
    public static final String STATUS_OK = String.valueOf(Response.Status.OK.getStatusCode());

    @Before
    public void setUp() throws Exception {
        statsHolder = new StatsHolder();
        concreteRef = (StatsHolder) statsHolder;
        responseStatusMap = new HashMap<>();
        concreteRef.setResponseStatusMap(responseStatusMap);
    }

    @After
    public void tearDown() throws Exception {
        statsHolder = null;
        concreteRef = null;
        responseStatusMap = null;
    }

    @Test
    public void testAddTestResult_singleResult_resultCounted() throws Exception {
        statsHolder.addTestResult(NTripleURLParserTest.URL_COMPLEX, STATUS_OK);
        assertEquals(1, responseStatusMap.size());
        assertEquals(new Integer(1), responseStatusMap.get(STATUS_OK));
    }

    @Test
    public void testAddTestResult_multipleResults_resultsCounted() throws Exception {
        initMultipleValidURLs();
        assertEquals(1, responseStatusMap.size());
        assertEquals(new Integer(2), responseStatusMap.get(STATUS_OK));
    }

    @Test
    public void testGetStats_multipleCodes() throws Exception {
        initMultipleValidURLs();
        long nowInMillis = Calendar.getInstance().getTimeInMillis();
        concreteRef.setLastURLTestTimestamp(nowInMillis);

        StatsDTO stats = statsHolder.getResponseCodeStats();
        assertEquals(NTripleURLParserTest.URL_SIMPLE, stats.getLastURLaccessed());
        assertEquals(2, stats.getTotalURLsProcessed());
        assertEquals(1, stats.getCodeStats().size());
        assertEquals(STATUS_OK, stats.getCodeStats().get(0).getCode());
        assertEquals(new Integer(2), stats.getCodeStats().get(0).getCount());
        String expectedTimestamp = StatsDTO.format.format(new Date(nowInMillis));
        assertEquals(expectedTimestamp, stats.getLastURLaccessTime());
    }

    private void initMultipleValidURLs() {
        statsHolder.addTestResult(NTripleURLParserTest.URL_COMPLEX, STATUS_OK);
        statsHolder.addTestResult(NTripleURLParserTest.URL_SIMPLE, STATUS_OK);

    }

}