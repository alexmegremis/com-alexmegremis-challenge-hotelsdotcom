package com.alexmegremis.challenges.hotelsdotcom;

import com.alexmegremis.challenges.hotelsdotcom.urlstats.FileReader;
import com.alexmegremis.challenges.hotelsdotcom.urlstats.IFileReader;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import java.io.IOException;

/**
 * This is the application starter.
 * We just spin up a Spring Boot up with whatever command line args.
 * <p>
 * Created by alexmegremis on 28/06/2016.
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {
    public static void main(String[] args) throws IOException {

        if(args.length == 0) {// || !Paths.get(args[0]).toFile().exists()) {
            System.out.println("Please provide an application argument for the NT file location as \"--ntfile=<relative-or-absolute-path-to-file>\" .");
            System.exit(0);
        }

        Application application = new Application();
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Application.class);

        application.configure(builder).run(args);
        FileReader fileReader = (FileReader) builder.context().getBean(IFileReader.class);
        fileReader.begin();
    }
}
