package com.alexmegremis.challenges.hotelsdotcom.api.v1.dto;

import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * This is the container that we'll use for our REST API.
 * It just to prettify the results.
 *
 * Created by alexmegremis on 28/06/2016.
 */
public class StatsDTO {

    public static final SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");

    @Getter @Setter
    private List<ResponseCodeStatDTO> codeStats;
    @Getter @Setter
    private String lastURLaccessed;
    @Getter @Setter
    private long totalURLsProcessed;

    private String lastURLaccessTime;

    public void setLastURLaccessTime(final long timeInMillis) {
        Date date = new Date(timeInMillis);
        this.lastURLaccessTime = format.format(date);
    }

    public String getLastURLaccessTime() {
        return lastURLaccessTime;
    }
}
