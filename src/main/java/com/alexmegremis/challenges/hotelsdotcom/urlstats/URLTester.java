package com.alexmegremis.challenges.hotelsdotcom.urlstats;

import javax.ws.rs.HttpMethod;
import java.net.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * This is a worker thread class that will test a URL and then store
 * the HTTP code in an external DTO.
 * <p>
 * <em>Note: </em> This implementation does not follow redirects, instead
 * counting them as 30x results.
 * <p>
 * The collection is passed as a constructor argument, and must be a
 * concurrent-safe implementation.
 * <p>
 * The DTO is also a constructor argument, and should be concurrent-safe.
 * <p>
 * Created by alexmegremis on 27/06/16.
 */
public class URLTester implements IURLTester {

    // @TODO parameterize this
    public static final int POLL_TIMEOUT_MS = 100;

    private BlockingQueue<String> urlQueue;
    private IStatsHolder statsHolder;


    private String nextURL = "";
    private URL url;
    private HttpURLConnection connection;

    static {
        HttpURLConnection.setFollowRedirects(false);
    }

    /**
     * Constructor through which we set our external data containers.
     *
     * @param urlQueue
     * @param statsHolder
     */
    public URLTester(final BlockingQueue<String> urlQueue, final IStatsHolder statsHolder) {
        this.urlQueue = urlQueue;
        this.statsHolder = statsHolder;
    }

    /**
     * When the thread kicks off, it will constantly try to pull a new URL off of the
     * provided queue.
     * <p>
     * The URL is tested, the result stored in the DTO, and the cycle repeats.
     * We timeout each test at 5sec.
     * <p>
     * We keep testing indefinitely.
     * <p>
     * Forever.
     * <p>
     * Until the end of time.
     * <p>
     * Even after winter comes.
     * <p>
     * And the white walkers are among us.
     * <p>
     * Unless we're interrupted.
     */
    @Override
    public void run() {
        boolean repeat = true;

        while (repeat) {

            try {
                nextURL = urlQueue.poll(POLL_TIMEOUT_MS, TimeUnit.MILLISECONDS);
//              nextURL = urlQueue.take();
                if (nextURL == null && Thread.currentThread().isInterrupted()) {
                    break;
                }
            } catch (InterruptedException e) {
                if (nextURL == null) {
                    break;
                } else {
                    repeat = false;
                }
            }

            testURL(nextURL);

            if (Thread.currentThread().isInterrupted()) {
                break;
            }
        }
//        System.out.println(">> Worker exiting");
    }

    /**
     * {@inheritDoc}
     * <p>
     * This is where we produce actual results for URL tests.
     * <p>
     * Miscellaneous connection exceptions are mapped to error text,
     * as these won't actually produce HTTP error codes.
     * <p>
     * These results are eventually recorded in the externally provided {@link BlockingQueue}
     * <p>
     *
     * @param nextURL
     */
    @Override
    public void testURL(final String nextURL) {

        connection = null;

        try {
            url = new URL(nextURL);
            connection = (HttpURLConnection) url.openConnection();
        } catch (ClassCastException e) {
            statsHolder.addTestResult(nextURL, CODE_FTP_URL);
        } catch (MalformedURLException | IllegalArgumentException e) {
            statsHolder.addTestResult(nextURL, CODE_BAD_URL);
        } catch (Exception e) {
            e.printStackTrace();
            statsHolder.addTestResult(nextURL, CODE_MISCELLANEOUS_ERROR);
        }

        if (connection != null) {
            try {
                connection.setRequestMethod(HttpMethod.HEAD);
                // @TODO parameterize this
                connection.setConnectTimeout(2000);
                connection.connect();
                int result = connection.getResponseCode();

                statsHolder.addTestResult(nextURL, String.valueOf(result));
            } catch (UnknownHostException e) {
                statsHolder.addTestResult(nextURL, CODE_UNKNOWN_HOST);
            } catch (SocketTimeoutException e) {
                statsHolder.addTestResult(nextURL, CODE_TIMEOUT);
            } catch (SocketException e) {
                statsHolder.addTestResult(nextURL, CODE_RESET);
            } catch (Exception e) {
                e.printStackTrace();
                statsHolder.addTestResult(nextURL, CODE_MISCELLANEOUS_ERROR);
            }
        }
    }
}
