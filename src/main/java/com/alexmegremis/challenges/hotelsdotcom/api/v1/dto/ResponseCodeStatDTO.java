package com.alexmegremis.challenges.hotelsdotcom.api.v1.dto;

import lombok.Data;

/**
 * Created by alexmegremis on 28/06/2016.
 *
 * We're being a bit verbose by creating this class, rather
 * than capture the same information in a simple straight up
 * map, because it human-reads better in the REST response.
 */
@Data
public class ResponseCodeStatDTO {
    private String code;
    private Integer count;

    public ResponseCodeStatDTO(final String code, final Integer count) {
        this.code = code;
        this.count = count;
    }
}
